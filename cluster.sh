#!/usr/bin/env bash
set -o errexit

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

while getopts c:a:d: flag
do
  case "${flag}" in
    c) command=${OPTARG};;
    a) args=${OPTARG};;
    d) dir=${OPTARG};;
    *) printf "${RED}Invalid option: -$flag ${NOCOLOR}\n" && ./scripts/help.sh && exit;;
  esac
done

case "$command" in 
  ("setup") ./scripts/setup/setup.sh ${args} ${dir} ;;
  ("cleanup") ./scripts/cleanup/cleanup.sh ${args} ;;
  ("installMetallb") ./scripts/installMetallb.sh ;;
  ("unpause") ./scripts/unpause.sh ${args} ;;
  ("pause") ./scripts/pause.sh ${args} ;;
  ("setupMinikube") ./scripts/setup/setupMinikube.sh ${args} ;;
  ("cleanupMinikube") ./scripts/cleanup/cleanupMinikube.sh ${args} ;;
  ("setupHosts") ./scripts/setup/setupHosts.sh ${args} ;;
  ("cleanupHosts") ./scripts/cleanup/cleanupHosts.sh ;;
  (*) ./scripts/help.sh ;;
esac