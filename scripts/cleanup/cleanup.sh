#!/usr/bin/env bash
# One parameter required: environment (kind or something minikube)
environment=$1

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

function delete_cleanup_log () {
  files=("cleanup.log")
  for file in "${files[@]}"; do
    if [[ -f "$file" ]] ; then
        rm "$file"
    fi
  done
}

function delete_artifacts () {
  ./scripts/cleanup/cleanupArtifacts.sh
}

function cleanup_kind_cluster () {
  ./scripts/cleanup/cleanupKind.sh $1
}

function cleanup_minikube_cluster () {
  ./scripts/cleanup/cleanupArgoCd.sh
  ./scripts/cleanup/cleanupSecureCodeBox.sh $1
  ./scripts/cleanup/cleanupCiCd.sh $1
  ./scripts/cleanup/cleanupMonitoring.sh
  ./scripts/cleanup/cleanupGitlab.sh $1
  ./scripts/cleanup/cleanupCluster.sh
  ./scripts/cleanup/cleanupMinio.sh $1
  ./scripts/cleanup/cleanupCertManager.sh
}

function cleanup_microk8s_cluster () {
  ./scripts/cleanup/cleanupArgoCd.sh
  ./scripts/cleanup/cleanupSecureCodeBox.sh $1
  ./scripts/cleanup/cleanupCiCd.sh $1
  ./scripts/cleanup/cleanupMonitoring.sh
  ./scripts/cleanup/cleanupGitlab.sh $1
  ./scripts/cleanup/cleanupCluster.sh
  ./scripts/cleanup/cleanupMinio.sh $1
  ./scripts/cleanup/cleanupCertManager.sh
}

function cleanup_local_cluster () {
  ./scripts/cleanup/cleanupArgoCd.sh
  ./scripts/cleanup/cleanupSecureCodeBox.sh $1
  ./scripts/cleanup/cleanupCiCd.sh $1
  ./scripts/cleanup/cleanupMonitoring.sh
  ./scripts/cleanup/cleanupGitlab.sh $1
  ./scripts/cleanup/cleanupCluster.sh
  ./scripts/cleanup/cleanupMinio.sh $1
  ./scripts/cleanup/cleanupCertManager.sh
  ./scripts/cleanup/cleanupIngress.sh
}

function cleanup () {
  if [[ "$1" != 200 ]] && [[ "$1" == "kind" ]]; then
    cleanup_kind_cluster $1
  elif [[ "$1" != 200 ]] && [[ "$1" == "minikube" ]]; then
    cleanup_minikube_cluster $1
  elif [[ "$1" != 200 ]] && [[ "$1" == "microk8s" ]]; then
    cleanup_microk8s_cluster $1
  elif [[ "$1" != 200 ]] && [[ "$1" == "local" ]]; then
    cleanup_local_cluster $1
  else
    printf "${RED}Environment $1 unknown. Use 'kind', 'minikube', 'microk8s' or 'local'.${NOCOLOR}\n"
  fi
}


printf "${BLUE}\n"
printf "Run cleanup script"
printf "${NOCOLOR}\n"
delete_cleanup_log
delete_artifacts
cleanup $environment
printf "\n${GREEN}Finished cleanup script.${NOCOLOR}\n"