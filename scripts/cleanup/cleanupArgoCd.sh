#!/usr/bin/env bash

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

function remove_finalizers () {
  (
    kubectl get AppProject -n argocd -o name | sed -e 's/.*\///g' | xargs -I {} kubectl patch AppProject -n argocd {} --type=json -p='[ { "op": "remove", "path": "/metadata/finalizers" } ]'
  ) >> cleanup.log 2>> cleanup.log
}

function delete_apps_app () {
  (
    kubectl exec -n argocd $(kubectl --namespace argocd get pods -lapp.kubernetes.io/name=argocd-server -o name) -- argocd app delete apps --cascade
  ) >> cleanup.log 2>> cleanup.log
}

function delete_apps () {
  (
    for app in $(argocd app list -o name); do 
      if [[ $app != 'projects' && $app != 'apps' ]]; then
        kubectl exec -n argocd $(kubectl --namespace argocd get pods -lapp.kubernetes.io/name=argocd-server -o name) -- argocd app delete $app --cascade
      fi
    done
  ) >> cleanup.log 2>> cleanup.log
}

function delete_project_app () {
  (
    kubectl exec -n argocd $(kubectl --namespace argocd get pods -lapp.kubernetes.io/name=argocd-server -o name) -- argocd app delete projects --cascade
    sleep 10
  ) >> cleanup.log 2>> cleanup.log
}

function delete_namespace () {
  (kubectl delete namespace argocd) >> cleanup.log 2>> cleanup.log
}

function delete_clusterrolebindings () {
  (
    kubectl delete clusterrolebinding argocd-server
    kubectl delete clusterrolebinding argocd-application-controller
  ) >> cleanup.log 2>> cleanup.log
}

function delete_clusterroles () {
  (
    kubectl delete clusterrole argocd-server
    kubectl delete clusterrole argocd-application-controller
  ) >> cleanup.log 2>> cleanup.log
}

function delete_customresourcedefinitions () {
  (
    kubectl delete customresourcedefinition applications.argoproj.io
    kubectl delete customresourcedefinition appprojects.argoproj.io
    kubectl delete customresourcedefinition argocdextensions.argoproj.io
    kubectl delete customresourcedefinition applicationsets.argoproj.io
  ) >> cleanup.log 2>> cleanup.log
}

printf "${BLUE}\n"
printf "Uninstall ArgoCD"
printf "${NOCOLOR}\n"
printf "   Delete finalizers of projects..."
remove_finalizers
printf "\r   Delete finalizers of projects...${GREEN}done${NOCOLOR}\n"
printf "   Delete app of apps..."
delete_apps_app
printf "\r   Delete app of apps...${GREEN}done${NOCOLOR}\n"
printf "   Delete apps..."
delete_apps
printf "\r   Delete apps...${GREEN}done${NOCOLOR}\n"
printf "   Delete projects app..."
delete_project_app
printf "\r   Delete namespaces...${GREEN}done${NOCOLOR}\n"
printf "\r   Delete projects app...${GREEN}done${NOCOLOR}\n"
printf "   Delete namespace..."
delete_namespace
printf "   Delete cluster role bindings..."
delete_clusterrolebindings
printf "\r   Delete cluster role bindings...${GREEN}done${NOCOLOR}\n"
printf "   Delete cluster roles..."
delete_clusterroles
printf "\r   Delete cluster roles...${GREEN}done${NOCOLOR}\n"
printf "   Delete custom resource definitions..."
delete_customresourcedefinitions
printf "\r   Delete custom resource definitions...${GREEN}done${NOCOLOR}\n"
printf "   ${GREEN}Uninstall ArgoCD successfully.${NOCOLOR}\n"