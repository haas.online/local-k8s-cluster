#!/usr/bin/env bash

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

function delete_files () {
  (  
    files=("dashboard-token.txt" "gitlab-root-password.txt" "gitlab-gitops-password.txt" "gitlab-kubernetes-cluster.txt" "argocd-admin-password.txt" "minio-jwt.txt" "setup.log" "setup.log" "defectdojo.txt")
    for file in "${files[@]}"; do
      if [[ -f "$file" ]] ; then
          rm "$file"
      fi
    done
  ) >> cleanup.log 2>> cleanup.log
}

function delete_dirs () {
  (
    directories=("temp")
    for directory in "${directories[@]}"; do
      if [[ -e "$directory" ]] ; then
        rm -rf "$directory"
      fi
    done
  ) >> cleanup.log 2>> cleanup.log
}

printf "${BLUE}\n"
printf "Delete artifacts"
printf "${NOCOLOR}\n"
printf "   Delete files..."
delete_files
printf "\r   Delete files...${GREEN}done${NOCOLOR}\n"
printf "   Delete directories..."
delete_dirs
printf "\r   Delete directories...${GREEN}done${NOCOLOR}\n"
printf "   ${GREEN}Delete artifacts successfully.${NOCOLOR}\n"