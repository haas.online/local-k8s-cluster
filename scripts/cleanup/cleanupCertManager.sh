#!/usr/bin/env bash

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

function delete_namespace () {
  (kubectl delete namespace cert-manager) >> cleanup.log 2>> cleanup.log
}

function delete_clusterissuer () {
  (kubectl delete clusterissuer ca-issuer) >> cleanup.log 2>> cleanup.log
}

function delete_clusterrolebindings () {
  (
    kubectl delete clusterrolebinding cert-manager-webhook:subjectaccessreviews
    kubectl delete clusterrolebinding cert-manager-controller-orders
    kubectl delete clusterrolebinding cert-manager-controller-issuers
    kubectl delete clusterrolebinding cert-manager-controller-ingress-shim
    kubectl delete clusterrolebinding cert-manager-controller-clusterissuers
    kubectl delete clusterrolebinding cert-manager-controller-challenges
    kubectl delete clusterrolebinding cert-manager-controller-certificatesigningrequests
    kubectl delete clusterrolebinding cert-manager-controller-certificates
    kubectl delete clusterrolebinding cert-manager-controller-approve:cert-manager-io
    kubectl delete clusterrolebinding cert-manager-cainjector
  ) >> cleanup.log 2>> cleanup.log
}

function delete_clusterroles () {
  (
    kubectl delete clusterrole cert-manager-webhook:subjectaccessreviews
    kubectl delete clusterrole cert-manager-view
    kubectl delete clusterrole cert-manager-edit
    kubectl delete clusterrole cert-manager-controller-orders
    kubectl delete clusterrole cert-manager-controller-issuers
    kubectl delete clusterrole cert-manager-controller-ingress-shim
    kubectl delete clusterrole cert-manager-controller-clusterissuers
    kubectl delete clusterrole cert-manager-controller-challenges
    kubectl delete clusterrole cert-manager-controller-certificatesigningrequests
    kubectl delete clusterrole cert-manager-controller-certificates
    kubectl delete clusterrole cert-manager-controller-approve:cert-manager-io
    kubectl delete clusterrole cert-manager-cainjector
  ) >> cleanup.log 2>> cleanup.log                       
}

function delete_mutatingwebhookconfiguration () {
  (kubectl delete mutatingwebhookconfiguration cert-manager-webhook) >> cleanup.log 2>> cleanup.log
}

function delete_validatingwebhookconfiguration () {
  (kubectl delete validatingwebhookconfiguration cert-manager-webhook) >> cleanup.log 2>> cleanup.log
}

function delete_customresourcedefinitions () {
  (
    kubectl delete customresourcedefinition certificaterequests.cert-manager.io
    kubectl delete customresourcedefinition certificates.cert-manager.io
    kubectl delete customresourcedefinition challenges.acme.cert-manager.io
    kubectl delete customresourcedefinition clusterissuers.cert-manager.io
    kubectl delete customresourcedefinition issuers.cert-manager.io
    kubectl delete customresourcedefinition orders.acme.cert-manager.io
  ) >> cleanup.log 2>> cleanup.log
}

printf "${BLUE}\n"
printf "Uninstall cert-manager"
printf "${NOCOLOR}\n"
printf "   Delete namespace..."
delete_namespace
printf "\r   Delete namespace...${GREEN}done${NOCOLOR}\n"
printf "   Delete cluster issuer..."
delete_clusterissuer
printf "\r   Delete cluster issuer...${GREEN}done${NOCOLOR}\n"
printf "   Delete cluster role bindings..."
delete_clusterrolebindings
printf "\r   Delete cluster role bindings...${GREEN}done${NOCOLOR}\n"
printf "   Delete cluster roles..."
delete_clusterroles
printf "\r   Delete cluster roles...${GREEN}done${NOCOLOR}\n"
printf "   Delete mutating webhook configuration..."
delete_mutatingwebhookconfiguration
printf "\r   Delete mutating webhook configuration...${GREEN}done${NOCOLOR}\n"
printf "   Delete validating webhook configuration..."
delete_validatingwebhookconfiguration
printf "\r   Delete validating webhook configuration...${GREEN}done${NOCOLOR}\n"
printf "   Delete custom resource definitions..."
delete_customresourcedefinitions
printf "\r   Delete custom resource definitions...${GREEN}done${NOCOLOR}\n"
printf "   ${GREEN}Uninstall cert-manager successfully.${NOCOLOR}\n"