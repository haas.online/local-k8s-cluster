#!/usr/bin/env bash

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

function delete_namespaces () {
  (
    kubectl delete namespace kubernetes-dashboard
    kubectl delete namespace keycloak
  ) >> cleanup.log 2>> cleanup.log
}

function delete_clusterrolebinding () {
  (kubectl delete clusterrolebinding dashboard-kubernetes-dashboard-metrics) >> cleanup.log 2>> cleanup.log
}

function delete_clusterrole () {
  (kubectl delete clusterrole dashboard-kubernetes-dashboard-metrics) >> cleanup.log 2>> cleanup.log
}

printf "${BLUE}\n"
printf "Cleanup cluster"
printf "${NOCOLOR}\n"
printf "   Delete namespaces..."
delete_namespaces
printf "\r   Delete persistent volumes...${GREEN}done${NOCOLOR}\n"
printf "   Delete cluster role binding..."
delete_clusterrolebinding
printf "\r   Delete cluster role binding...${GREEN}done${NOCOLOR}\n"
printf "   Delete cluster role..."
delete_clusterrole
printf "\r   Delete cluster role...${GREEN}done${NOCOLOR}\n"
printf "   ${GREEN}Cleanup cluster successfully.${NOCOLOR}\n"