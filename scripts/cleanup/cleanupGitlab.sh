#!/usr/bin/env bash

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

environment=$1

function delete_namespace () {
  (kubectl delete namespace gitlab) >> cleanup.log 2>> cleanup.log
}

function delete_secrets () {
  (
    kubectl delete secret gitlab-gitlab-shell-host-keys
    kubectl delete secret gitlab-gitlab-shell-secret
    kubectl delete secret gitlab-gitaly-secret
    kubectl delete secret gitlab-praefect-secret
    kubectl delete secret gitlab-rails-secret
    kubectl delete secret gitlab-gitlab-workhorse-secret
    kubectl delete secret gitlab-gitlab-runner-secret
    kubectl delete secret gitlab-gitlab-kas-secret
    kubectl delete secret gitlab-minio-secret
    kubectl delete secret gitlab-postgresql-password
    kubectl delete secret gitlab-gitlab-pages-secret
    kubectl delete secret gitlab-registry-httpsecret
    kubectl delete secret gitlab-registry-notification
    kubectl delete secret gitlab-praefect-dbsecret
  ) >> cleanup.log 2>> cleanup.log
}

function delete_clusterrolebinding () {
  (kubectl delete clusterrolebinding gitlab-admin) >> cleanup.log 2>> cleanup.log
}

function delete_persistentvolumes () {
    if [[ "$1" != 200 ]] && [[  "$1" == "minikube" ]]; then
      (kubectl delete -f ./kubernetes/gitlab/persistent-volumes.yaml) >> ./cleanup.log 2>> ./cleanup.log
      return 0
    elif [[ "$1" != 200 ]] && [[ "$1" == "kind" || "$1" == "microk8s" || "$1" == "local" ]]; then
      printf "\n   ${YELLOW}Persistent volumes are not installed on KinD, microK8s or local. Skip step.${NOCOLOR}\n"
      return 1
    else
      printf "   ${RED}\nEnvironment $1 unknown. Use 'kind', 'minikube', 'microk8s' or 'local'.${NOCOLOR}\n"
      return 1
    fi
}

printf "${BLUE}\n"
printf "Uninstall gitlab"
printf "${NOCOLOR}\n"
printf "   Delete namespace..."
delete_namespace
printf "\r   Delete namespace...${GREEN}done${NOCOLOR}\n"
printf "   Delete secrets..."
delete_secrets
printf "\r   Delete secrets...${GREEN}done${NOCOLOR}\n"
printf "   Delete cluster role binding..."
delete_clusterrolebinding
printf "\r   Delete cluster role binding...${GREEN}done${NOCOLOR}\n"
printf "   Delete persistent volumes..."
delete_persistentvolumes $environment
printf "\r   Delete persistent volumes...${GREEN}done${NOCOLOR}\n"
printf "   ${GREEN}Uninstall gitlab successfully.${NOCOLOR}\n"