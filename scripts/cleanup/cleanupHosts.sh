#!/usr/bin/env bash

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

function cleanup_hosts_on_linux () {
  (  
    cp /etc/hosts ./hosts.new
    sed -i '/dashboard.local/d' ./hosts.new
    sed -i '/gitlab.local/d' ./hosts.new
    sed -i '/argocd.local/d' ./hosts.new
    sed -i '/prometheus.local/d' ./hosts.new
    cat ./hosts.new | sudo tee -i /etc/hosts
    rm ./hosts.new
  ) >> cleanup.log 2>> cleanup.log
}

function cleanup_hosts_on_mac () {
  (
    cp /etc/hosts ./hosts.new
    sed -i '' '/dashboard.local/d' ./hosts.new
    sed -i '' '/gitlab.local/d' ./hosts.new
    sed -i '' '/sonarqube.local/d' ./hosts.new
    sed -i '' '/argocd.local/d' ./hosts.new
    sed -i '' '/prometheus.local/d' ./hosts.new
    cat ./hosts.new | sudo tee -i /etc/hosts
    rm ./hosts.new
  ) >> cleanup.log 2>> cleanup.log
}

function cleanup_hosts_on_windows () {
  (  
    cp /c/Windows/System32/drivers/etc/hosts ./hosts.new
    sed -i '/dashboard.local/d' ./hosts.new
    sed -i '/gitlab.local/d' ./hosts.new
    sed -i '/sonarqube.local/d' ./hosts.new
    sed -i '/argocd.local/d' ./hosts.new
    sed -i '/prometheus.local/d' ./hosts.new
    cat ./hosts.new | tee -i /c/Windows/System32/drivers/etc/hosts
    rm ./hosts.new
  ) >> cleanup.log 2>> cleanup.log
}

function cleanup_hosts () {
  if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    cleanup_hosts_on_linux
    return 0
  elif [[ "$OSTYPE" == "darwin"* ]]; then
    cleanup_hosts_on_mac
    return 0
  elif [[ "$OSTYPE" == "msys" ]]; then
    cleanup_hosts_on_windows
    return 0
  elif [[ "$OSTYPE" == "cygwin" ]]; then
    printf "   ${RED}Cleanup hosts for cygwin not yet implemented.${NOCOLOR}\n"
    return 1
  elif [[ "$OSTYPE" == "freebsd"* ]]; then
    printf "   ${RED}Cleanup hosts for freebsd not yet implemented.${NOCOLOR}\n"
    return 1
  else
    printf "   ${RED}OS unknown to cleanup hosts script.${NOCOLOR}\n"
    return 1
  fi
}

printf "${BLUE}\n"
printf "Cleanup hosts file"
printf "${NOCOLOR}\n"
printf "   Remove local k8s cluster entries..."
cleanup_hosts
if [[ $? == 0 ]]; then 
  printf "\r   Remove local k8s cluster entries...${GREEN}done${NOCOLOR}\n"
  printf "   ${GREEN}Cleanup hosts successfully.${NOCOLOR}\n"
fi