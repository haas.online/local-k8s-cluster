#!/usr/bin/env bash

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

function uninstall_nginx () {
  (helm uninstall nginx-ingress) >> cleanup.log 2>> cleanup.log
}

printf "${BLUE}\n"
printf "Uninstall nginx"
printf "${NOCOLOR}\n"
printf "   Uninstall nginx helm release..."
uninstall_nginx
printf "\r   Uninstall nginx helm release...${GREEN}done${NOCOLOR}\n"
printf "   ${GREEN}Uninstall nginx successfully.\n${NOCOLOR}"

