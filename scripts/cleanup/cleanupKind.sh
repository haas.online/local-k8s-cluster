#!/usr/bin/env bash
set -o errexit

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

# One parameter required: environment (kind or something minikube)
environment=$1

function cleanup_kind () {
  if [[ "$environment" != 200 && "$environment" == "kind" ]]; then
    (
      docker rm -f local-k8s-cluster-control-plane
      docker rm -f local-k8s-cluster-worker
      docker rm -f local-k8s-cluster-worker2
      docker rm -f local-k8s-cluster-worker3
      docker rm -f local-k8s-registry
    ) >> cleanup.log 2>> cleanup.log
  fi
}

printf "${BLUE}\n"
printf "Cleanup KinD cluster"
printf "${NOCOLOR}\n"
printf "   Remove containers..."
cleanup_kind
printf "\r   Remove containers...${GREEN}done${NOCOLOR}\n"
printf "   ${GREEN}Cleanup KinD cluster successfully.${NOCOLOR}\n"