#!/usr/bin/env bash
set -o errexit

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

function delete_minikube () {
  minikube delete
}

function cleanup_host_dns () {
  if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    printf "${YELLOW}Delete Minikube for linux not yet implemented (step cleanup host dns).${NOCOLOR}\n"
  elif [[ "$OSTYPE" == "darwin"* ]]; then
    (    
      if [[ -f /etc/resolver/minikube-local ]]; then 
        sudo rm -f /etc/resolver/minikube-local
      fi
    ) >> cleanup.log 2>> cleanup.log
  elif [[ "$OSTYPE" == "cygwin" ]]; then
    printf "${YELLOW}Delete Minikube for cygwin not yet implemented (step cleanup host dns).${NOCOLOR}\n"
  elif [[ "$OSTYPE" == "msys" ]]; then
    printf "${YELLOW}Delete Minikube for msys not yet implemented (step cleanup host dns).${NOCOLOR}\n"
  elif [[ "$OSTYPE" == "freebsd"* ]]; then
    printf "${YELLOW}Delete Minikube for freebsd not yet implemented (step cleanup host dns).${NOCOLOR}\n"
  else
    printf "${YELLOW}OS unknown to delete Minikube script (step cleanup host dns).${NOCOLOR}\n"
  fi
}

function cleanup_hosts () {
  # Update hosts file
  (sudo ./cluster.sh -c cleanupHosts) >> cleanup.log 2>> cleanup.log
}

printf "${BLUE}\n"
printf "Delete Minikube cluster"
printf "${NOCOLOR}\n"
printf "   Delete Minikube cluster...\n"
delete_minikube
printf "   Cleanup host dns..."
cleanup_host_dns
printf "\r   Cleanup host dns...${GREEN}done${NOCOLOR}\n"
printf "   Cleanup hosts file..."
cleanup_hosts
printf "\r   Cleanup hosts file...${GREEN}done${NOCOLOR}\n"
printf "   ${GREEN}Delete Minikube cluster successfully.${NOCOLOR}\n"
