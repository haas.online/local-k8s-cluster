#!/usr/bin/env bash

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

environment=$1

function delete_namespaces () {
  (
    kubectl delete namespace minio-tenant-1
    kubectl delete namespace minio-operator
  ) >> cleanup.log 2>> cleanup.log
}

function delete_storageclass () {
  (kubectl delete storageclass minio) >> cleanup.log 2>> cleanup.log
}

function delete_persistentvolumes () {
    if [[ "$1" != 200 ]] && [[  "$1" == "minikube" ]]; then
      (kubectl delete -f ./kubernetes/minio/persistent-volumes.yaml) >> ./cleanup.log 2>> ./cleanup.log
      return 0
    elif [[ "$1" != 200 ]] && [[ "$1" == "kind" || "$1" == "microk8s" || "$1" == "local" ]]; then
      printf "\n   ${YELLOW}Persistent volumes are not installed on KinD, microK8s or local. Skip step.${NOCOLOR}\n"
      return 1
    else
      printf "   ${RED}\nEnvironment $1 unknown. Use 'kind', 'minikube', 'microk8s' or 'local'.${NOCOLOR}\n"
      return 1
    fi
}

function delete_clusterrolebindings () {
  (
    kubectl delete clusterrolebinding console-sa-binding
    kubectl delete clusterrolebinding minio-operator-binding
  ) >> cleanup.log 2>> cleanup.log
}

function delete_clusterroles () {
  (
    kubectl delete clusterrole console-sa-role
    kubectl delete clusterrole minio-operator-role
  ) >> cleanup.log 2>> cleanup.log
}

function delete_customresourcedefinition () {
  (
    kubectl delete customresourcedefinition tenants.minio.min.io
  ) >> cleanup.log 2>> cleanup.log
}

function cleanup_minio () {
  printf "${BLUE}\n"
  printf "Uninstall minio"
  printf "${NOCOLOR}\n"
  printf "   Delete namespace..."
  delete_namespaces
  printf "\r   Delete namespace...${GREEN}done${NOCOLOR}\n"
  printf "   Delete storageclass..."
  delete_storageclass
  printf "\r   Delete storageclass...${GREEN}done${NOCOLOR}\n"
  printf "   Delete persistent volumes..."
  delete_persistentvolumes $environment
  if [[ $? == 0 ]]; then
    printf "\r   Delete persistent volumes...${GREEN}done${NOCOLOR}\n"
  fi
  printf "   Delete cluster role bindings..."
  delete_clusterrolebindings
  printf "\r   Delete cluster role bindings...${GREEN}done${NOCOLOR}\n"
  printf "   Delete cluster roles..."
  delete_clusterroles
  printf "\r   Delete cluster roles...${GREEN}done${NOCOLOR}\n"
  printf "   Delete custom resource definitions..."
  delete_customresourcedefinition
  printf "\r   Delete custom resource definitions...${GREEN}done${NOCOLOR}\n"
  printf "   ${GREEN}Uninstall minio successfully.${NOCOLOR}\n"
}

if [[ "$environment" != 200 ]] && [[ "$environment" == "kind" ]]; then
  cleanup_minio
elif [[ "$environment" != 200 ]] && [[ "$environment" == "minikube" ]]; then
  cleanup_minio
elif [[ "$environment" != 200 ]] && [[ "$environment" == "microk8s" ]]; then
  printf "\n${YELLOW}Min-IO is not installed on microk8s. Cleanup skipped.${NOCOLOR}\n"
elif [[ "$environment" != 200 ]] && [[ "$environment" == "local" ]]; then
  cleanup_minio
else
  printf "\n${RED}Environment $environment unknown. Use 'kind', 'minikube', 'microk8s' or 'local'.${NOCOLOR}\n"
fi