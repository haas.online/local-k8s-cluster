#!/usr/bin/env bash

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

function delete_fix () {
  ( 
    # Due to the following issue https://github.com/prometheus-community/helm-charts/issues/1500
    # There is a workaround implemented according to the following blog entry:
    # https://blog.ediri.io/kube-prometheus-stack-and-argocd-how-workarounds-are-born
    kubectl delete -k ./kubernetes/monitoring/
  ) >> cleanup.log 2>> cleanup.log
}

function delete_namespace () {
  (kubectl delete namespace monitoring) >> cleanup.log 2>> cleanup.log
}

function delete_clusterrolebindings () {
  (
    kubectl delete clusterrolebinding kube-prometheus-stack-prometheus
    kubectl delete clusterrolebinding kube-prometheus-stack-operator
    kubectl delete clusterrolebinding kube-prometheus-stack-kube-state-metrics
    kubectl delete clusterrolebinding kube-prometheus-stack-grafana-clusterrolebinding
  ) >> cleanup.log 2>> cleanup.log
}

function delete_clusterroles () {
  (
    kubectl delete clusterrole kube-prometheus-stack-prometheus
    kubectl delete clusterrole kube-prometheus-stack-operator
    kubectl delete clusterrole kube-prometheus-stack-kube-state-metrics
    kubectl delete clusterrole kube-prometheus-stack-grafana-clusterrole
  ) >> cleanup.log 2>> cleanup.log
}

function delete_mutatingwebhookconfiguration () {
  (kubectl delete mutatingwebhookconfiguration kube-prometheus-stack-admission) >> cleanup.log 2>> cleanup.log
}

function delete_validatingwebhookconfiguration () {
  (kubectl delete validatingwebhookconfiguration kube-prometheus-stack-admission) >> cleanup.log 2>> cleanup.log
}

function delete_podsecuritypolicy () {
  (kubectl delete podsecuritypolicy prometheus-blackbox-exporter-psp) >> cleanup.log 2>> cleanup.log
}

function delete_customresourcedefinitions () {
  (
    kubectl delete customresourcedefinition alertmanagerconfigs.monitoring.coreos.com
    kubectl delete customresourcedefinition alertmanagers.monitoring.coreos.com
    kubectl delete customresourcedefinition podmonitors.monitoring.coreos.com
    kubectl delete customresourcedefinition probes.monitoring.coreos.com
    kubectl delete customresourcedefinition prometheuses.monitoring.coreos.com
    kubectl delete customresourcedefinition prometheusrules.monitoring.coreos.com
    kubectl delete customresourcedefinition servicemonitors.monitoring.coreos.com
    kubectl delete customresourcedefinition thanosrulers.monitoring.coreos.com
  ) >> cleanup.log 2>> cleanup.log
}

printf "${BLUE}\n"
printf "Uninstall Monitoring"
printf "${NOCOLOR}\n"
printf "   Delete fix..."
delete_fix
printf "\r   Delete fix...${GREEN}done${NOCOLOR}\n"
printf "   Delete namespace..."
delete_namespace
printf "\r   Delete namespace...${GREEN}done${NOCOLOR}\n"
printf "   Delete cluster role bindings..."
delete_clusterrolebindings
printf "\r   Delete cluster role bindings...${GREEN}done${NOCOLOR}\n"
printf "   Delete cluster roles..."
delete_clusterroles
printf "\r   Delete cluster roles...${GREEN}done${NOCOLOR}\n"
printf "   Delete mutating webhook configuration..."
delete_mutatingwebhookconfiguration
printf "\r   Delete mutating webhook configuration...${GREEN}done${NOCOLOR}\n"
printf "   Delete validating webhook configuration..."
delete_validatingwebhookconfiguration
printf "\r   Delete validating webhook configuration...${GREEN}done${NOCOLOR}\n"
printf "   Delete pod security policy..."
delete_podsecuritypolicy
printf "\r   Delete pod security policy...${GREEN}done${NOCOLOR}\n"
printf "   Delete custom resource definitions..."
delete_customresourcedefinitions
printf "\r   Delete custom resource definitions...${GREEN}done${NOCOLOR}\n"
printf "   ${GREEN}Uninstall Monitoring successfully.${NOCOLOR}\n"