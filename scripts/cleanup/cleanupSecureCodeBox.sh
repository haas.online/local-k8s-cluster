#!/usr/bin/env bash

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

environment=$1

function remove_finalizers () {
  (
    kubectl get scans -n securecodebox-scanner -o name | sed -e 's/.*\///g' | xargs -I {} kubectl patch scans -n securecodebox-scanner {} --type=json -p='[ { "op": "remove", "path": "/metadata/finalizers" } ]'
  ) >> cleanup.log 2>> cleanup.log
}

function delete_namespaces () {
  (
    kubectl delete namespace securecodebox-system
    kubectl delete namespace securecodebox-scanner
    kubectl delete namespace defectdojo
  ) >> cleanup.log 2>> cleanup.log
}

function delete_defectdojo_secrets () {
  (
    kubectl delete secrets -n defectdojo defectdojo defectdojo-redis-specific defectdojo-rabbitmq-specific defectdojo-postgresql-specific defectdojo-mysql-specific
  ) >> cleanup.log 2>> cleanup.log
}

function delete_defectdojo_serviceaccount () {
  (
    kubectl delete serviceAccount defectdojo
  ) >> cleanup.log 2>> cleanup.log
}

function delete_defectdojo_clusterrole () {
  (
    kubectl delete clusterrole dd-job-role
  ) >> cleanup.log 2>> cleanup.log
}

function delete_defectdojo_persistentvolumeclaims () {
  (
    kubectl delete pvc data-defectdojo-rabbitmq-0 data-defectdojo-postgresql-0 data-defectdojo-mysql-0
  ) >> cleanup.log 2>> cleanup.log
}

function delete_persistentvolume () {
    if [[ "$1" != 200 ]] && [[  "$1" == "minikube" ]]; then
      (kubectl delete -f ./kubernetes/securecodebox/persistent-volumes.yaml) >> ./cleanup.log 2>> ./cleanup.log
      return 0
    elif [[ "$1" != 200 ]] && [[ "$1" == "kind" || "$1" == "microk8s" || "$1" == "local" ]]; then
      printf "\n   ${YELLOW}Persistent volumes are not installed on KinD, microK8s or local. Skip step.${NOCOLOR}\n"
      return 1
    else
      printf "   ${RED}\nEnvironment $1 unknown. Use 'kind', 'minikube', 'microk8s' or 'local'.${NOCOLOR}\n"
      return 1
    fi
}

function delete_customresourcedefinitions () {
  (
    kubectl delete customresourcedefinition cascadingrules.cascading.securecodebox.io
    kubectl delete customresourcedefinition parsedefinitions.execution.securecodebox.io
    kubectl delete customresourcedefinition scancompletionhooks.execution.securecodebox.io
    kubectl delete customresourcedefinition scans.execution.securecodebox.io
    kubectl delete customresourcedefinition scantypes.execution.securecodebox.io
    kubectl delete customresourcedefinition scheduledscans.execution.securecodebox.io
  ) >> cleanup.log 2>> cleanup.log
}

printf "${BLUE}\n"
printf "Uninstall secure code box"
printf "${NOCOLOR}\n"
printf "   Delete finalizers of scans..."
remove_finalizers
printf "\r   Delete finalizers of scans...${GREEN}done${NOCOLOR}\n"
printf "   Delete namespaces..."
delete_namespaces
printf "\r   Delete namespaces...${GREEN}done${NOCOLOR}\n"
printf "   Delete DefectDojo secrets..."
delete_defectdojo_secrets
printf "\r   Delete DefectDojo secrets...${GREEN}done${NOCOLOR}\n"
printf "   Delete DefectDojo service account..."
delete_defectdojo_serviceaccount
printf "\r   Delete DefectDojo service account...${GREEN}done${NOCOLOR}\n"
printf "   Delete DefectDojo cluster role..."
delete_defectdojo_clusterrole
printf "\r   Delete DefectDojo cluster role...${GREEN}done${NOCOLOR}\n"
printf "   Delete DefectDojo persistent volume claims..."
delete_defectdojo_persistentvolumeclaims
printf "\r   Delete DefectDojo persistent volume claims...${GREEN}done${NOCOLOR}\n"
printf "   Delete persistent volumes..."
delete_persistentvolume $environment
printf "\r   Delete persistent volumes...${GREEN}done${NOCOLOR}\n"
printf "   Delete custom resource definitions..."
delete_customresourcedefinitions
printf "\r   Delete custom resource definitions...${GREEN}done${NOCOLOR}\n"
printf "   ${GREEN}Uninstall secure code box successfully.${NOCOLOR}\n"