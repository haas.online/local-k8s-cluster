#!/usr/bin/env bash
set -o errexit

BLUE='\033[1;34m'
NOCOLOR='\033[0m'

printf "${BLUE}\n"
printf "The cluster script can be invokes in the following way:\n"
printf "  sh cluster.sh -c command [-a argument]\n"
printf "\n"
printf "Available commands:\n"
printf "\n"
printf "  setup - Initializes the local k8s cluster. Needs -a 'environment', either kind, minikube or microk8s and -d 'directory' as base path for shared folder with host.\n"
printf "  cleanup - Removes the local k8s cluster and related files. Needs -a 'environment' as argument, either kind, minikube or microk8s.\n"
printf "  pause - Pauses the containers of the local k8s cluster. Needs -a 'environment' as argument, either kind or minikube.\n"
printf "  unpause - Unpauses the containers of the local k8s cluster. Needs -a 'environment' as argument, either kind or minikube. Run in separate terminal for minikube.\n"
printf "  installMetallb - Installs the metallb (load balancer). Optional for kind environment.\n"
printf "  setupMinikube - Init a new minikube with suitable configurations. Run in separate Terminal.\n"
printf "  cleanupMinikube - Deletes the minikube.\n"
printf "  setupHosts - Adds hosts entries for k8s ingresses. Needs -a 'environment' as argument, either kind, minikube or microk8s.\n"
printf "  cleanupHosts - Removes hosts entries of k8s ingresses.\n"
printf "${NOCOLOR}\n"