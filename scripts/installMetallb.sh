#!/usr/bin/env bash
set -o errexit

YELLOW='\033[1;33m'
NOCOLOR='\033[0m'

kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/master/manifests/namespace.yaml
kubectl apply -f ./kubernetes/cluster/metallb.yaml
kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)" 
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/master/manifests/metallb.yaml

printf "\n"
printf "${YELLOW}\n"
printf "Wait for pods' ready status in metallb-system namespace.\n"
printf "${NOCOLOR}\n"

kubectl wait -n metallb-system \
  --for=condition=ready pod \
  --selector=app=metallb \
  --timeout=240s