#!/usr/bin/env bash

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

function delete_public_ingerss_class () {
  (
    kubectl delete ingressclass public
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function replace_ingress_class () {
  (
    kubectl apply -f ./kubernetes/cluster/microk8s-ingressclass-replacement.yaml
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

printf "${BLUE}\n"
printf "Replace ingress class for microk8s to be compatible with other nginx setups."
printf "${NOCOLOR}\n"
printf "   Delete public ingress class..."
delete_public_ingerss_class
printf "\r   Delete public ingress class...${GREEN}done${NOCOLOR}\n"
printf "   Apply nginx ingress class..."
replace_ingress_class
printf "\r   Applied nginx ingress class...${GREEN}done${NOCOLOR}\n"
printf "   ${GREEN}Replaced ingress class for microk8s to be compatible with other nginx setups successfully.${NOCOLOR}\n"