#!/usr/bin/env bash
set -o errexit

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

environment=$1
directory=$2

function create_temp_dir () {
  # setup temp folder
  if ! [[ -e temp ]] ; then
    mkdir temp
  fi
}

function setup_cluster_environment () {
  if [[ "$1" != 200 ]] && [[ "$1" == "kind" ]]; then
    ./scripts/setup/setupKind.sh $2
  elif [[ "$1" != 200 ]] && [[ "$1" == "microk8s" ]]; then
    ./scripts/setup/replaceIngressClass.sh
  elif [[ "$1" != 200 ]] && [[ "$1" == "local" ]]; then
    ./scripts/setup/setupIngress.sh
  fi
}

function setup_workload () {
  ./scripts/setup/setupCertManager.sh
  ./scripts/setup/setupMinio.sh $1
  ./scripts/setup/setupCluster.sh
  ./scripts/setup/setupGitlab.sh $1
  ./scripts/setup/setupArgoCd.sh
  ./scripts/setup/setupMonitoring.sh
  ./scripts/setup/setupCiCD.sh $1
  ./scripts/setup/setupSecureCodeBox.sh $1
}


if [[ "$environment" != 200 ]] && [[ "$environment" == "kind" || "$environment" == "minikube" || "$environment" == "microk8s" || "$environment" == "local" ]]; then
  create_temp_dir
  setup_cluster_environment ${environment} ${directory}
  setup_workload ${environment}
else
  printf "${RED}Environment $environment unknown. Use 'kind', 'minikube', 'microk8s' or 'local'.${NOCOLOR}\n"
fi