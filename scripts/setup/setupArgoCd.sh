#!/usr/bin/env bash
set -o errexit

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

token_value=$(head -c 20 gitlab-root-password.txt)
private_key=$(cat ./temp/id_ed25519)
hostname=gitlab.local

function push_argo_repository () {
  (
    # Push argocd repository
    cp -R ./argocd ./temp/argocd
    cd ./temp/argocd
    git init
    git switch -c master
    git remote add origin https://gitops:$token_value@gitlab.local/gitops/argocd.git
    git add .
    git commit -m "Initial commit."
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function install_argo () {
  (
    # Install argocd without authentication
    helm repo add argo https://argoproj.github.io/argo-helm
    helm repo update
    helm upgrade --install argocd argo/argo-cd -n argocd --create-namespace \
      -f ./kubernetes/argocd/values.yaml \
      --set configs.credentialTemplates.ssh-creds.sshPrivateKey="$private_key" \
      --version 3.35.4
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function wait_for_argo () {
  (  
    kubectl wait --namespace argocd \
      --for=condition=ready pod \
      --selector=app.kubernetes.io/name=argocd-server \
      --timeout=1200s
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function patch_argo_deployment () {
  (
    kubectl patch deploy argocd-server -n argocd -p '[{"op": "add", "path": "/spec/template/spec/containers/0/command/-", "value": "--disable-auth"}]' --type json
    kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function static_wait () {
  sleep $1
}

function setup_argo_on_linux_and_mac () {
  (
    static_wait 20
    argocd_password=$(kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo)
    # Login argocd cli inside argocd server pod
    kubectl exec -n argocd $(kubectl --namespace argocd get pods -lapp.kubernetes.io/name=argocd-server -o name) -- /bin/bash -c "argocd login localhost:8080 --insecure --username admin --password $argocd_password"
    # Add git to known hosts
    kubectl exec -n argocd $(kubectl --namespace argocd get pods -lapp.kubernetes.io/name=argocd-server -o name) -- /bin/bash -c "ssh-keyscan -p 32022 gitlab-gitlab-shell.gitlab.svc.cluster.local | argocd cert add-ssh --batch --grpc-web --upsert"
    # Copy private ssh key for argocd into argocd server pod
    kubectl cp -n argocd ./temp/id_ed25519 $(kubectl --namespace argocd get pods -lapp.kubernetes.io/name=argocd-server -o=jsonpath='{.items[0].metadata.name}'):/tmp/id_ed25519
    static_wait 60
    # Create a new project with name "argocd"
    curl -k -H "Content-Type:application/json" -d "{ \"name\": \"argocd\" }" "https://gitlab.local/api/v4/projects?private_token=$token_value"
    # Commit to project "argocd"
    (
      cd ./temp/argocd
      git -c http.sslVerify=false push --set-upstream origin master
    )
    # Add repo to argocd
    kubectl exec -n argocd $(kubectl --namespace argocd get pods -lapp.kubernetes.io/name=argocd-server -o name) -- argocd repo add ssh://git@gitlab-gitlab-shell.gitlab.svc.cluster.local:32022/gitops/argocd.git --name gitlab.local --ssh-private-key-path /tmp/id_ed25519 --upsert
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function setup_argo_on_windows () {
  (
    static_wait 20
    argocd_password=$(kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo)
    kubectl exec -n argocd $(kubectl --namespace argocd get pods -lapp.kubernetes.io/name=argocd-server -o name) -- argocd login localhost:8080 --insecure --username admin --password $argocd_password
    kubectl exec -n argocd $(kubectl --namespace argocd get pods -lapp.kubernetes.io/name=argocd-server -o name) -- //bin//bash -c "ssh-keyscan -p 32022 gitlab-gitlab-shell.gitlab.svc.cluster.local | argocd cert add-ssh --batch --grpc-web --upsert"
    kubectl cp -n argocd ./temp/id_ed25519 $(kubectl --namespace argocd get pods -lapp.kubernetes.io/name=argocd-server -o=jsonpath='{.items[0].metadata.name}'):/tmp/id_ed25519
    static_wait 60
    curl -k -H "Content-Type:application/json" -d "{ \"name\": \"argocd\" }" "https://gitlab.local/api/v4/projects?private_token=$token_value"
    (
      cd ./temp/argocd
      git -c http.sslVerify=false push --set-upstream origin master
    )
    kubectl exec -n argocd $(kubectl --namespace argocd get pods -lapp.kubernetes.io/name=argocd-server -o name) -- argocd repo add ssh://git@gitlab-gitlab-shell.gitlab.svc.cluster.local:32022/gitops/argocd.git --name gitlab.local --ssh-private-key-path //tmp//id_ed25519 --upsert
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function  setup_argo () {
  if [[ "$OSTYPE" == "linux-gnu"* || "$OSTYPE" == "darwin"* ]]; then
    setup_argo_on_linux_and_mac
    return 0
  elif [[ "$OSTYPE" == "msys" ]]; then
    setup_argo_on_windows
    return 0
  elif [[ "$OSTYPE" == "cygwin" ]]; then
    printf "${RED}Setup ArgoCD for cygwin not yet implemented.${NOCOLOR}\n"
    return 1
  elif [[ "$OSTYPE" == "freebsd"* ]]; then
    printf "${RED}Setup ArgoCD for freebsd not yet implemented.${NOCOLOR}\n"
    return 1
  else
    printf "${RED}OS unknown to setup ArgoCD script.${NOCOLOR}\n"
    return 1
  fi
}

function print_argo_admin_password () {
  # Save initial admin password for argocd
  argocd_password=$(kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d)
  printf "\n"
  printf "${GREEN}"
  printf "   %s" $argocd_password
  printf "${NOCOLOR}"
  printf "\n\n"
  printf "${YELLOW}   Argo CD admin password is stored in argocd-admin-password.txt${NOCOLOR}"
  printf "\n"
  echo $argocd_password > argocd-admin-password.txt
}

printf "${BLUE}\n"
printf "Install ArgoCD"
printf "${NOCOLOR}\n"
printf "   Push ArgoCD repositor to gitlab..."
push_argo_repository
printf "\r   Push ArgoCD repositor to gitlab...${GREEN}done${NOCOLOR}\n"
printf "   Install ArgoCD from helm chart..."
install_argo
printf "\r   Install ArgoCD from helm chart...${GREEN}done${NOCOLOR}\n"
printf "   Wait for ArgoCD to be up and running..."
wait_for_argo
printf "\r   Wait for ArgoCD to be up and running...${GREEN}done${NOCOLOR}\n"
printf "   Patch ArgoCD deployment..."
patch_argo_deployment
printf "\r   Patch ArgoCD deployment...${GREEN}done${NOCOLOR}\n"
printf "   Wait for ArgoCD to be up and running..."
wait_for_argo
printf "\r   Wait for ArgoCD to be up and running...${GREEN}done${NOCOLOR}\n"
printf "   Setup ArgoCD instance..."
setup_argo
if [[ $? == 0 ]]; then
  printf "\r   Setup ArgoCD instance...${GREEN}done${NOCOLOR}\n"
fi
printf "   Print ArgoCD admin password...\n"
print_argo_admin_password
printf "   ${GREEN}Install ArgoCD successfully.${NOCOLOR}\n"