#!/usr/bin/env bash
set -o errexit

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

function create_ca_cert_on_linux () {
  (
    cd temp
    openssl req -nodes -new -x509 -keyout ca.key -out ca.crt -subj "/C=DE/ST=Baden-Wurttemberg/L=Stuttgart/OU=Development/CN=*.local/emailAddress=haas.test.stg@gmail.com"
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function create_ca_cert_on_mac () {
  (
    cd temp
    cp /etc/ssl/openssl.cnf openssl-with-ca.cnf

    cat << EOF >> openssl-with-ca.cnf
    [ v3_ca ]
    basicConstraints = critical,CA:TRUE
    subjectKeyIdentifier = hash
    authorityKeyIdentifier = keyid:always,issuer:always
EOF

    openssl req -nodes -new -x509 -keyout ca.key -out ca.crt -subj "/C=DE/ST=Baden-Wurttemberg/L=Stuttgart/OU=Development/CN=*.local/emailAddress=haas.test.stg@gmail.com" -extensions v3_ca -config openssl-with-ca.cnf
    sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain ./ca.crt
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function create_ca_cert_on_windows () {
  (
    cd temp
    openssl req -nodes -new -x509 -keyout ca.key -out ca.crt -subj "/C=DE/ST=Baden-Wurttemberg/L=Stuttgart/OU=Development/CN=*.local/emailAddress=haas.test.stg@gmail.com"
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function create_ca_cert () {
  if [[ "$OSTYPE" = "linux-gnu"* ]]; then
    create_ca_cert_on_linux
    return 0
  elif [[ "$OSTYPE" = "darwin"* ]]; then
    create_ca_cert_on_mac
    return 0
  elif [[ "$OSTYPE" = "msys" ]]; then
    create_ca_cert_on_windows
    return 0
  elif [[ "$OSTYPE" = "cygwin" ]]; then
    printf "${RED}Setup cert-manager for cygwin not yet implemented.${NOCOLOR}\n"
    return 1
  elif [[ "$OSTYPE" = "freebsd"* ]]; then
    printf "${RED}Setup cert-manager for freebsd not yet implemented.${NOCOLOR}\n"
    return 1
  else
    printf "${RED}OS unknown to setup cert-manager script.${NOCOLOR}\n"
    return 1
  fi
}

function create_tls_secret () {
  (
    # Create TLS secret
    cd temp; \
    kubectl create namespace cert-manager; \
    kubectl create secret tls ca-key-pair -n cert-manager --key="ca.key" --cert="ca.crt"
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function install_cert_manager () {
  (  
    # Install Cert-Manager for root certificate for a custom PKI (Public Key Infrastructure)
    # kubectl patch deployment ingress-nginx-controller -n ingress-nginx -p '{"spec": {"template":{"metadata":{"annotations":{"cert-manager.io/cluster-issuer": "selfsigned-issuer"}}}} }'

    helm repo add jetstack https://charts.jetstack.io
    helm repo update
    helm install \
      cert-manager jetstack/cert-manager \
      --namespace cert-manager \
      --create-namespace \
      --version v1.7.2 \
      --set installCRDs=true
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function wait_for_cert_manager () {
  (
    kubectl wait --namespace cert-manager \
      --for=condition=ready pod\
      --selector=app.kubernetes.io/component=webhook \
      --timeout=600s
    kubectl wait --namespace cert-manager \
      --for=condition=ready pod\
      --selector=app.kubernetes.io/component=cainjector \
      --timeout=600s
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function create_issuer () {
  (kubectl apply -f ./kubernetes/cert-manager/cert-manager.yaml -n cert-manager) >> ./temp/setup.log 2>> ./temp/setup.log
}

printf "${BLUE}\n"
printf "Install cert-manager"
printf "${NOCOLOR}\n"
printf "   Create CA certificate..."
create_ca_cert
if [[ $? == 0 ]]; then
  printf "\r   Create CA certificate...${GREEN}done${NOCOLOR}\n"
fi
printf "   Create TLS secret..."
create_tls_secret
printf "\r   Create TLS secret...${GREEN}done${NOCOLOR}\n"
printf "   Install cert-manager from helm chart..."
install_cert_manager
printf "\r   Install cert-manager from helm chart...${GREEN}done${NOCOLOR}\n"
printf "   Wait for cert-manager to be up and running..."
wait_for_cert_manager
printf "\r   Wait for cert-manager to be up and running...${GREEN}done${NOCOLOR}\n"
printf "   Create issuer of cert-manager..."
create_issuer
printf "\r   Create issuer of cert-manager...${GREEN}done${NOCOLOR}\n"
printf "   ${GREEN}Cert-manager installed successfully.${NOCOLOR}\n"