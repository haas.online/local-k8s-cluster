#!/usr/bin/env bash

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

environment=$1

function create_persistentvolumes () {
    if [[ "$1" != 200 ]] && [[  "$1" == "minikube" ]]; then
      (kubectl apply -f ./kubernetes/ci-cd/persistent-volumes.yaml) >> ./temp/setup.log 2>> ./temp/setup.log
      return 0
    elif [[ "$1" != 200 ]] && [[ "$1" == "kind" || "$1" == "microk8s" || "$1" == "local" ]]; then
      printf "\n   ${YELLOW}Persistent volumes are not installed on KinD, microK8s or local. Skip step.${NOCOLOR}\n"
      return 1
    else
      printf "    ${RED}\nEnvironment $1 unknown. Use 'kind', 'minikube', 'microk8s' or 'local'.${NOCOLOR}\n"
      return 1
    fi
}

printf "${BLUE}\n"
printf "Setup CI/CD"
printf "${NOCOLOR}\n"
printf "   Create persistent volumes..."
create_persistentvolumes $environment
if [[ $? == 0 ]]; then
  printf "\r   Create persistent volumes...${GREEN}done${NOCOLOR}\n"
fi
printf "   ${GREEN}Setup CI/CD successfully.${NOCOLOR}\n"