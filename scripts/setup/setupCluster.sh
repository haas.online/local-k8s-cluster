#!/usr/bin/env bash
set -o errexit

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

function install_dashboard () {
  (
    helm repo add kubernetes-dashboard https://kubernetes.github.io/dashboard/
    helm upgrade --install dashboard kubernetes-dashboard/kubernetes-dashboard -n kubernetes-dashboard --create-namespace \
      -f kubernetes/cluster/dashboard-values.yaml \
      --timeout 600s
    kubectl apply -f ./kubernetes/cluster/dashboard-rbac.yaml
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function wait_for_dashboard () {
  (
    kubectl wait --namespace kubernetes-dashboard \
      --for=condition=ready pod \
      --selector=app.kubernetes.io/component=kubernetes-dashboard \
      --timeout=420s
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function print_dashboard_token () {
  dashboard_token=$(kubectl -n kubernetes-dashboard create token admin-user)

  printf "${GREEN}\n   "
  printf "   %s" $dashboard_token
  printf "\n\n"
  printf "${YELLOW}   Dashboard-Token is stored inside dashboard-token.txt\n${NOCOLOR}"
  kubectl -n kubernetes-dashboard create token admin-user
  printf "%s" $dashboard_token >> dashboard-token.txt
}

printf "${BLUE}\n"
printf "Setup cluster wide components"
printf "${NOCOLOR}\n"
printf "   Install dashboard from helm chart..."
install_dashboard
printf "\r   Install dashboard from helm chart...${GREEN}done${NOCOLOR}\n"
printf "   Wait for dashboard to be up and running..."
wait_for_dashboard
printf "\r   Wait for dashboard to be up and running...${GREEN}done${NOCOLOR}\n"
printf "   Print out dashboard token...\n"
print_dashboard_token
printf "   ${GREEN}Setup cluster wide components successfully.${NOCOLOR}\n"