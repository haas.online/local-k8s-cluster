#!/usr/bin/env bash

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

environment=$1

function prepare_install () {
  (
    helm repo add gitlab https://charts.gitlab.io/
    helm repo update
    kubectl create namespace gitlab
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function create_persistentvolumes () {
    if [[ "$1" != 200 ]] && [[  "$1" == "minikube" ]]; then
      (kubectl apply -f ./kubernetes/gitlab/persistent-volumes.yaml) >> ./temp/setup.log 2>> ./temp/setup.log
      return 0
    elif [[ "$1" != 200 ]] && [[ "$1" == "kind" || "$1" == "microk8s" || "$1" == "local" ]]; then
      printf "\n   ${YELLOW}Persistent volumes are not installed on KinD, microK8s or local. Skip step.${NOCOLOR}\n"
      return 1
    else
      printf "   ${RED}\nEnvironment $1 unknown. Use 'kind', 'minikube', 'microk8s' or 'local'.${NOCOLOR}\n"
      return 1
    fi
}

function create_host_keys () {
  (
  # Create host keys
    mkdir -p temp/hostKeys
    ssh-keygen -t rsa  -f temp/hostKeys/ssh_host_rsa_key -N ""
    ssh-keygen -t dsa  -f temp/hostKeys/ssh_host_dsa_key -N ""
    ssh-keygen -t ecdsa  -f temp/hostKeys/ssh_host_ecdsa_key -N ""
    ssh-keygen -t ed25519  -f temp/hostKeys/ssh_host_ed25519_key -N ""
    kubectl create secret generic gitlab-gitlab-shell-host-keys --from-file temp/hostKeys
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function create_secrets () {
  (
    # Create gitlab shell secret
    kubectl create secret generic gitlab-gitlab-shell-secret --from-literal=secret=$(head -c 512 /dev/urandom | LC_CTYPE=C tr -cd 'a-zA-Z0-9' | head -c 64)

    # Create gitaly secret
    kubectl create secret generic gitlab-gitaly-secret --from-literal=token=$(head -c 512 /dev/urandom | LC_CTYPE=C tr -cd 'a-zA-Z0-9' | head -c 64)
  
    # Create praefect secret
    kubectl create secret generic gitlab-praefect-secret --from-literal=token=$(head -c 512 /dev/urandom | LC_CTYPE=C tr -cd 'a-zA-Z0-9' | head -c 64)

    # Create Rails secret
    (
    mkdir temp/gitlab-rails; \
    cd temp/gitlab-rails; \
    cat << EOF > secrets.yaml
production:
  secret_key_base: $(head -c 512 /dev/urandom | LC_CTYPE=C tr -cd 'a-zA-Z0-9' | head -c 128)
  otp_key_base: $(head -c 512 /dev/urandom | LC_CTYPE=C tr -cd 'a-zA-Z0-9' | head -c 128)
  db_key_base: $(head -c 512 /dev/urandom | LC_CTYPE=C tr -cd 'a-zA-Z0-9' | head -c 128)
  encrypted_settings_key_base: $(head -c 512 /dev/urandom | LC_CTYPE=C tr -cd 'a-zA-Z0-9' | head -c 128)
  openid_connect_signing_key: |
$(openssl genrsa 2048 | awk '{print "    " $0}')
  ci_jwt_signing_key: |
$(openssl genrsa 2048 | awk '{print "    " $0}')
EOF
    )

    kubectl create secret generic gitlab-rails-secret --from-file=temp/gitlab-rails/secrets.yaml

    # Create Workhorse key
    kubectl create secret generic gitlab-gitlab-workhorse-secret --from-literal=shared_secret=$(head -c 512 /dev/urandom | LC_CTYPE=C tr -cd 'a-zA-Z0-9' | head -c 32 | base64)

    # Create Runner secret
    kubectl create secret generic gitlab-gitlab-runner-secret --from-literal=runner-registration-token=$(head -c 512 /dev/urandom | LC_CTYPE=C tr -cd 'a-zA-Z0-9' | head -c 64)

    # Create KAS secret
    kubectl create secret generic gitlab-gitlab-kas-secret --from-literal=kas_shared_secret=$(head -c 512 /dev/urandom | LC_CTYPE=C tr -cd 'a-zA-Z0-9' | head -c 32 | base64)

    # Create minio secret
    kubectl create secret generic gitlab-minio-secret \
        --from-literal=accesskey=$(head -c 512 /dev/urandom | LC_CTYPE=C tr -cd 'a-zA-Z0-9' | head -c 20) \
        --from-literal=secretkey=$(head -c 512 /dev/urandom | LC_CTYPE=C tr -cd 'a-zA-Z0-9' | head -c 64)

    # Create PostgreSQL credentials
    kubectl create secret generic gitlab-postgresql-password \
        --from-literal=postgresql-password=$(head -c 512 /dev/urandom | LC_CTYPE=C tr -cd 'a-zA-Z0-9' | head -c 64) \
        --from-literal=postgresql-postgres-password=$(head -c 512 /dev/urandom | LC_CTYPE=C tr -cd 'a-zA-Z0-9' | head -c 64)

    # Create Pages secret
    kubectl create secret generic gitlab-gitlab-pages-secret --from-literal=shared_secret=$(head -c 512 /dev/urandom | LC_CTYPE=C tr -cd 'a-zA-Z0-9' | head -c 32 | base64)

    # Create Registry http secret
    kubectl create secret generic gitlab-registry-httpsecret --from-literal=secret=$(head -c 512 /dev/urandom | LC_CTYPE=C tr -cd 'a-zA-Z0-9' | head -c 64)

    # Create Registry notification secret
    kubectl create secret generic gitlab-registry-notification --from-literal=secret=[\"$(head -c 512 /dev/urandom | LC_CTYPE=C tr -cd 'a-zA-Z0-9' | head -c 32)\"]

    # Create Praefect db secret
    kubectl create secret generic gitlab-praefect-dbsecret --from-literal=secret=$(head -c 512 /dev/urandom | LC_CTYPE=C tr -cd 'a-zA-Z0-9' | head -c 64)
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function install_gitlab_on_kind () {
  (
    helm upgrade --install gitlab gitlab/gitlab -n gitlab \
      -f kubernetes/gitlab/values-base.yaml \
      -f kubernetes/gitlab/values-ssl.yaml \
      --set service.externalPort=443
    
    kubectl apply -f kubernetes/gitlab/gitlab-admin-service-account.yaml
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function install_gitlab_on_minikube () {
  (
    helm upgrade --install gitlab gitlab/gitlab -n gitlab \
      -f kubernetes/gitlab/values-base.yaml \
      -f kubernetes/gitlab/values-ssl.yaml \
      --set global.hosts.externalIP=$(minikube ip) \
      --set service.externalPort=443
    
    kubectl apply -f kubernetes/gitlab/gitlab-admin-service-account.yaml
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function install_gitlab_on_microk8s () {
  (
    helm upgrade --install gitlab gitlab/gitlab -n gitlab \
      -f kubernetes/gitlab/values-base.yaml \
      -f kubernetes/gitlab/values-ssl.yaml \
      --set global.hosts.externalIP=$(microk8s kubectl cluster-info | grep -o -P '(?<=https://).*(?=:.*)' | head -1) \
      --set service.externalPort=443
    
    kubectl apply -f kubernetes/gitlab/gitlab-admin-service-account.yaml
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function install_gitlab_on_local () {
  (
    helm upgrade --install gitlab gitlab/gitlab -n gitlab \
      -f kubernetes/gitlab/values-base.yaml \
      -f kubernetes/gitlab/values-ssl.yaml \
      --set global.hosts.externalIP=$(kubectl cluster-info | grep -o -P '(?<=https://).*(?=:.*)' | head -1) \
      --set service.externalPort=443
    
    kubectl apply -f kubernetes/gitlab/gitlab-admin-service-account.yaml
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function install_gitlab () {
  if [[ "$environment" != 200 ]] && [[ "$environment" == "kind" ]]; then
    install_gitlab_on_kind
    return 0
  elif [[ "$environment" != 200 ]] && [[ "$environment" == "minikube" ]]; then
    install_gitlab_on_minikube
    return 0
  elif [[ "$environment" != 200 ]] && [[ "$environment" == "microk8s" ]]; then
    install_gitlab_on_microk8s
    return 0
  elif [[ "$environment" != 200 ]] && [[ "$environment" == "local" ]]; then
    install_gitlab_on_local
    return 0
  else
    printf "${RED}Environment $environment unknown. Use 'kind', 'minikube' or 'local'.${NOCOLOR}\n"
    return 1
  fi
}

function print_gitlab_root_password () {
  # Save initial root password for gitlab
  printf "\n"
  printf "${GREEN}"
  printf "   "
  printf $(kubectl get secret gitlab-gitlab-initial-root-password -ojsonpath='{.data.password}' -n gitlab | base64 --decode)
  printf "\n\n"
  printf "${YELLOW}   Root password is stored in gitlab-root-password.txt${NOCOLOR}"
  printf "\n"
  kubectl get secret gitlab-gitlab-initial-root-password -ojsonpath='{.data.password}' -n gitlab | base64 --decode > gitlab-root-password.txt
}

function wait_for_gitlab () {
  (  
    kubectl wait --namespace gitlab \
      --for=condition=ready pod \
      --selector=app=webservice \
      --timeout=2400s
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function install_gitlab_runner () {
  (
    # Install gitlab-runner
    helm upgrade --install gitlab-runner gitlab/gitlab-runner -n gitlab \
      -f kubernetes/gitlab-runner/values.yaml

    ssh-keygen -t ed25519 -C "gitops" -f ./temp/id_ed25519 -q -N ""
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function wait_for_toolbox () {
  (
    # Add gitops user and access token
    kubectl wait --namespace gitlab \
      --for=condition=ready pod \
      --selector=app=toolbox \
      --timeout=1200s
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function add_gitops_user () {  
  token=$(head -c 20 gitlab-root-password.txt)
  user_name='gitops'
  domain='gitlab.local'
  token_name='Automation-Token'
  e_mail="$user_name@$domain"
 
  ( 
    kubectl exec -n gitlab $(kubectl --namespace gitlab get pods -lapp=toolbox -o name) -- gitlab-rails runner "user = User.new(username: '$user_name', email: '$e_mail', name: '$user_name', password: '$token', password_confirmation: '$token'); user.skip_confirmation!; user.save!; token=user.personal_access_tokens.create(scopes: [:api, :sudo], name: '$token_name'); token.set_token('$token'); token.save!; print 'User and access token created'"
  ) >> ./temp/setup.log 2>> ./temp/setup.log

  # Save gitops user password
  printf "\n"
  printf "${GREEN}"
  printf "   "
  printf $token
  printf "\n\n"
  printf "${YELLOW}   Gitops user password is stored in gitlab-gitops-password.txt${NOCOLOR}"
  printf "\n"
  printf $token > gitlab-gitops-password.txt
}

function add_gitops_ssh_key () {
  (
    # Add default ssh-key to gitops user
    key_pub=$(cat ./temp/id_ed25519.pub)
    hostname=gitlab.local
    curl -k --data-urlencode "key=$key_pub" --data-urlencode "title=$hostname" "https://gitlab.local/api/v4/user/keys?private_token=$token"
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function save_cluster_info () {
  # Save kubernetes cluster info to register cluster inside gitlab
  printf "${YELLOW}"
  printf "   Cluster info to link cluster inside gitlab are stored in gitlab-kubernetes-cluster.txt\n"
  printf "${NOCOLOR}"
  (
    echo "Cluster-API-URL: https://kubernetes.default.svc" | tee gitlab-kubernetes-cluster.txt
    echo "Remember to allow request to local network (Settings > Network)" | tee -a gitlab-kubernetes-cluster.txt
    echo "" | tee -a gitlab-kubernetes-cluster.txt
    echo "" | tee -a gitlab-kubernetes-cluster.txt
    echo "CA-Certificate:" | tee -a gitlab-kubernetes-cluster.txt
    echo "" | tee -a gitlab-kubernetes-cluster.txt
    kubectl get secret $(kubectl -n gitlab get secret | grep default-token | awk '{print $1}') -n gitlab -o jsonpath="{['data']['ca\.crt']}" | base64 --decode | tee -a gitlab-kubernetes-cluster.txt
    echo "" | tee -a gitlab-kubernetes-cluster.txt
    echo "" | tee -a gitlab-kubernetes-cluster.txt
    echo "Service Token:" | tee -a gitlab-kubernetes-cluster.txt
    echo "" | tee -a gitlab-kubernetes-cluster.txt
    kubectl get secret $(kubectl -n kube-system get secret | grep gitlab | awk '{print $1}') -n kube-system -o go-template="{{.data.token | base64decode}}" | tee -a gitlab-kubernetes-cluster.txt
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

printf "${BLUE}\n"
printf "Install gitlab"
printf "${NOCOLOR}\n"
printf "   Prepare installation..."
prepare_install
printf "\r   Prepare installation...${GREEN}done${NOCOLOR}\n"
printf "   Create persistent volumes..."
create_persistentvolumes $environment
if [[ $? == 0 ]]; then
  printf "\r   Create persistent volumes...${GREEN}done${NOCOLOR}\n"
fi
printf "   Create host keys..."
create_host_keys
printf "\r   Create host keys...${GREEN}done${NOCOLOR}\n"
printf "   Create secrets..."
create_secrets
printf "\r   Create secrets...${GREEN}done${NOCOLOR}\n"
printf "   Install gitlab..."
install_gitlab
if [[ $? == 0 ]]; then
  printf "\r   Install gitlab...${GREEN}done${NOCOLOR}\n"
fi
printf "   Print out gitlab root password...\n"
print_gitlab_root_password
printf "   Wait for gitlab to be up and running..."
wait_for_gitlab
printf "\r   Wait for gitlab to be up and running...${GREEN}done${NOCOLOR}\n"
printf "   Install gitlab runner..."
install_gitlab_runner
printf "\r   Install gitlab runner...${GREEN}done${NOCOLOR}\n"
printf "   Wait for toolbox to be up and running..."
wait_for_toolbox
printf "\r   Wait for toolbox to be up and running...${GREEN}done${NOCOLOR}\n"
printf "   Add gitops user to gitlab...\n"
add_gitops_user
printf "   Add ssh key for gitops user to gitlab..."
add_gitops_ssh_key
printf "\r   Add ssh key for gitops user to gitlab...${GREEN}done${NOCOLOR}\n"
printf "   Save cluster info to add it later to gitlab...\n"
save_cluster_info
printf "   ${GREEN}Install gitlab successfully.${NOCOLOR}\n"