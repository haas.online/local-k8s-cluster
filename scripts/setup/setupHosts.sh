#!/usr/bin/env bash
set -o errexit

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

environment=$1

function create_temp_dir () {
  # setup temp folder
  if ! [[ -e temp ]] ; then
    mkdir temp
  fi
}

function extend_hosts_on_linux_and_mac () {
  (
    echo "$1     dashboard.local minio.local keycloak.local" | sudo tee -a /etc/hosts
    echo "$1     gitlab.local registry.gitlab.local minio.gitlab.local" | sudo tee -a /etc/hosts
    echo "$1     sonarqube.local defectdojo.local" | sudo tee -a /etc/hosts
    echo "$1     argocd.local" | sudo tee -a /etc/hosts
    echo "$1     prometheus.local grafana.local promlens.local blackbox-exporter.local" | sudo tee -a /etc/hosts
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function extend_hosts_on_windows () {  
  (
    echo "$1     dashboard.local minio.local keycloak.local" | tee -a /c/Windows/System32/drivers/etc/hosts
    echo "$1     gitlab.local registry.local minio.local" | tee -a /c/Windows/System32/drivers/etc/hosts
    echo "$1     sonarqube.local defectdojo.local" | tee -a /c/Windows/System32/drivers/etc/hosts
    echo "$1     argocd.local" | tee -a /c/Windows/System32/drivers/etc/hosts
    echo "$1     prometheus.local grafana.local promlens.local blackbox-exporter.local" | tee -a /c/Windows/System32/drivers/etc/hosts
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function extend_hosts () {
  if [[ "$environment" != 200 ]] && [[ "$environment" == "kind" ]]; then
    cluster_ip="127.0.0.1"
  elif [[ "$environment" != 200 ]] && [[ "$environment" == "minikube" ]]; then
    cluster_ip=$(minikube ip)
  elif [[ "$environment" != 200 ]] && [[ "$environment" == "microk8s" ]]; then
    cluster_ip=$(kubectl describe node | grep -o -P '(?<=InternalIP:).*' | awk '{$1=$1};1' | head -1)
  elif [[ "$environment" != 200 ]] && [[ "$environment" == "local" ]]; then
    cluster_ip=$(kubectl describe node | grep -o -P '(?<=InternalIP:).*' | awk '{$1=$1};1' | head -1)
  else
    printf "${RED}Environment $environment unknown. Use 'kind', 'minikube', 'microk8s' or 'local'.${NOCOLOR}\n"
    return 1
  fi

  if [[ "$OSTYPE" == "linux-gnu"* || "$OSTYPE" == "darwin"* ]]; then
    extend_hosts_on_linux_and_mac $cluster_ip
    return 0
  elif [[ "$OSTYPE" == "msys" ]]; then
    extend_hosts_on_windows $cluster_ip
    return 0
  elif [[ "$OSTYPE" == "cygwin" ]]; then
    printf "${RED}Setup hosts for cygwin not yet implemented.${NOCOLOR}\n"
    return 1
  elif [[ "$OSTYPE" == "freebsd"* ]]; then
    printf "${RED}Setup hosts for freebsd not yet implemented.${NOCOLOR}\n"
    return 1
  else
    printf "${RED}OS unknown to setup hosts script.${NOCOLOR}\n"
    return 1
  fi
}

printf "${BLUE}\n"
printf "Extend hosts file"
printf "${NOCOLOR}\n"
printf "   Add entries to hosts file..."
create_temp_dir
extend_hosts
if [[ $? == 0 ]]; then
  printf "\r   Add entries to hosts file...${GREEN}done${NOCOLOR}\n"
fi
printf "   ${GREEN}Extend hosts successfully.${NOCOLOR}\n"