#!/usr/bin/env bash
set -o errexit

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

function install_ingress () {
  (
    helm repo add nginx-stable https://helm.nginx.com/stable
    helm repo update
    helm install nginx-ingress nginx-stable/nginx-ingress
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

printf "${BLUE}\n"
printf "Install nginx ingress"
printf "${NOCOLOR}\n"
printf "   Install nginx from helm chart..."
install_ingress
printf "\r   Install nginx from helm chart...${GREEN}done${NOCOLOR}\n"
printf "   Install nginx successfully.\n"