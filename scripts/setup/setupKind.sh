#!/usr/bin/env bash
set -o errexit

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

directory=$1

function create_config () {
  (
    if [[ $directory != "" ]]; then
      control_plane_extramount="\n  extraMounts:\n  - hostPath: $directory/control-plane\n    containerPath: /data"
      worker_1_extramount="\n  extraMounts:\n  - hostPath: $directory/worker-1\n    containerPath: /data"
    fi

    cat << EOF > temp/config.yaml
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
name: local-k8s-cluster
networking:
  disableDefaultCNI: true
containerdConfigPatches:
- |-
  [plugins."io.containerd.grpc.v1.cri".registry.mirrors."localhost:5000"]
    endpoint = ["http://local-k8s-registry:5000"]
nodes:
- role: control-plane
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true" $(printf "$control_plane_extramount")
  extraPortMappings:
  - containerPort: 80
    hostPort: 80
    listenAddress: "0.0.0.0"
  - containerPort: 443
    hostPort: 443
    listenAddress: "0.0.0.0"
  - containerPort: 32022
    hostPort: 32022
    listenAddress: "0.0.0.0"
- role: worker $(printf "$worker_1_extramount")
EOF
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function create_registry () {
  (
    reg_name='local-k8s-registry'
    reg_port='5000'
    running="$(docker inspect -f '{{.State.Running}}' "${reg_name}" 2>/dev/null || true)"
    # create registry container unless it already exists
    if [[ "${running}" != 'true' ]]; then
      docker run \
        -d --restart=always -p "127.0.0.1:${reg_port}:5000" --name "${reg_name}" \
        registry:2
    fi
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function create_kind_cluster () {
  (
    # create a cluster with the local registry enabled in containerd
    # Setup is using reg_name local-k8s-registry and reg_port 5000
    kind create cluster --config=temp/config.yaml --image=kindest/node:v1.22.5
  )
}

function connect_registry () {
  (
    # connect the registry to the cluster network
    # (the network may already be connected)
    docker network connect "kind" "${reg_name}" || true

    # Document the local registry
    # https://github.com/kubernetes/enhancements/tree/master/keps/sig-cluster-lifecycle/generic/1755-communicating-a-local-registry
    kubectl apply -f ./kubernetes/cluster/local-registry-configmap.yaml
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function install_weave_net () {
  (
    # Install weave net
    curl -L "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')&env.IPALLOC_RANGE=10.10.0.0/16" --output ./temp/weave.yaml
    kubectl apply -f ./temp/weave.yaml
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function install_ingress () {
  (
    # Install ingress
    kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function wait_for_ingress () {
  (
    kubectl wait --namespace ingress-nginx \
      --for=condition=ready pod \
      --selector=app.kubernetes.io/component=controller \
      --timeout=1200s
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

printf "${BLUE}\n"
printf "Setup KinD cluster"
printf "${NOCOLOR}\n"
printf "   Create config.yaml..."
create_config
printf "\r   Create config.yaml...${GREEN}done${NOCOLOR}\n"
printf "   Create local registry..."
create_registry
printf "\r   Create local registry...${GREEN}done${NOCOLOR}\n"
printf "   Create KinD cluster...\n"
create_kind_cluster
printf "   Connect registry to KinD cluster..."
connect_registry
printf "\r   Connect registry to KinD cluster...${GREEN}done${NOCOLOR}\n"
printf "   Install weave net..."
install_weave_net
printf "\r   Install weave net...${GREEN}done${NOCOLOR}\n"
printf "   Install nginx ingress..."
install_ingress
printf "\r   Install nginx ingress...${GREEN}done${NOCOLOR}\n"
printf "   Wait for nginx ingress to be up and running..."
wait_for_ingress
printf "\r   Wait for nginx ingress to be up and running...${GREEN}done${NOCOLOR}\n"
printf "   ${GREEN}Setup KinD cluster successfully.${NOCOLOR}\n"