#!/usr/bin/env bash
set -o errexit

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

function create_temp_dir () {
  # setup temp folder
  if ! [[ -e temp ]] ; then
    mkdir temp
  fi
}

function start_minikube () {
  (
    minikube start --driver=hyperkit --container-runtime=docker --disk-size=256GB
    eval $(minikube docker-env)
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function enable_addons () {
  (
    minikube addons enable metallb
    minikube addons enable ingress
    minikube addons enable ingress-dns
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function configure_addons () {
  minikube addons configure metallb
}

function setup_host_dns_on_mac () {
  (
    minikube_ip=$(minikube ip)
    cat << EOF > minikube-local
domain local
nameserver $minikube_ip
search_order 1
timeout 5
EOF
    sudo mv minikube-local /etc/resolver/
    sudo killall -HUP mDNSResponder
    scutil --dns
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

setup_host_dns () {
  if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    printf "${YELLOW}Setup Minikube for linux not yet implemented (step setup host dns).${NOCOLOR}\n"
    return 1
  elif [[ "$OSTYPE" == "darwin"* ]]; then
    setup_host_dns_on_mac
    return 0
  elif [[ "$OSTYPE" == "cygwin" ]]; then
    printf "${YELLOW}Setup Minikube for cygwin not yet implemented (step setup host dns).${NOCOLOR}\n"
    return 1
  elif [[ "$OSTYPE" == "msys" ]]; then
    printf "${YELLOW}Setup Minikube for msys not yet implemented (step setup host dns).${NOCOLOR}\n"
    return 1
  elif [[ "$OSTYPE" == "freebsd"* ]]; then
    printf "${YELLOW}Setup Minikube for freebsd not yet implemented (step setup host dns).${NOCOLOR}\n"
    return 1
  else
    printf "${YELLOW}OS unknown to setup Minikube script (step setup host dns).${NOCOLOR}\n"
    return 1
  fi
}

function update_hosts_file () {
  (
    # Update hosts file
    sudo ./cluster.sh -c cleanupHosts
    sudo ./cluster.sh -c setupHosts -a minikube
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function setup_pv_directories () {
  (
    minikube ssh -- sudo mkdir \
        /data/minio-local-k8s-1 \
        /data/minio-local-k8s-2 \
        /data/minio-local-k8s-3 \
        /data/minio-local-k8s-4 \
        /data/minio-local-k8s-5 \
        /data/minio-local-k8s-6 \
        /data/gitlab-minio-pv \
        /data/gitlab-redis-pv \
        /data/gitlab-postgresql-pv \
        /data/gitlab-repo-data-pv \
        /data/securecodebox-operator-minio-pv \
        /data/defectdojo-postgresql-pv \
        /data/defectdojo-rabbitmq-pv \
        /data/sonarqube-postgresql-pv
    minikube ssh -- sudo chown -R dbus:root /data
    minikube ssh -- sudo chmod -R 777 /data
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

printf "${BLUE}\n"
printf "Setup Minikube cluster"
printf "${NOCOLOR}\n"
printf "   Create temp dir..."
create_temp_dir
printf "\r   Create temp dir...${GREEN}done${NOCOLOR}\n"
printf "   Start minikube...\n"
start_minikube
printf "   Enable addons..."
enable_addons
printf "\r   Enable addons...${GREEN}done${NOCOLOR}\n"
printf "   Configure addons...\n"
configure_addons
printf "   Setup host dns...\n"
setup_host_dns
if [[ $? == 0 ]]; then
  printf "\r   Setup host dns...${GREEN}done${NOCOLOR}\n"
fi
printf "   Update hosts file..."
update_hosts_file
printf "\r   Update hosts file...${GREEN}done${NOCOLOR}\n"
printf "   Setup persistent volume host directories (minikube vm)..."
setup_pv_directories
printf "\r   Setup persistent volume host directories (minikube vm)...${GREEN}done${NOCOLOR}\n"
printf "   ${GREEN}Setup Minikube cluster successfully${NOCOLOR}\n"