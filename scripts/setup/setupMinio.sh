#!/usr/bin/env bash

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

environment=$1

function prepare_setup () {
  (
    kubectl create namespace minio-tenant-1
    kubectl create namespace minio-operator
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function create_storageclass () {
  (kubectl apply -f ./kubernetes/minio/storage-class.yaml) >> ./temp/setup.log 2>> ./temp/setup.log
}

function create_persistentvolumes () {
    if [[ "$1" != 200 ]] && [[  "$1" == "minikube" ]]; then
      (kubectl apply -f ./kubernetes/minio/persistent-volumes.yaml) >> ./temp/setup.log 2>> ./temp/setup.log
      return 0
    elif [[ "$1" != 200 ]] && [[ "$1" == "kind" || "$1" == "microk8s" || "$1" == "local" ]]; then
      printf "\n   ${YELLOW}Persistent volumes are not installed on KinD, microK8s or local. Skip step.${NOCOLOR}\n"
      return 1
    else
      printf "   ${RED}\nEnvironment $1 unknown. Use 'kind', 'minikube', 'microk8s' or 'local'.${NOCOLOR}\n"
      return 1
    fi
}

function create_ingress () {
  (kubectl apply -f ./kubernetes/minio/ingress.yaml -n minio-operator) >> ./temp/setup.log 2>> ./temp/setup.log
}

function init_minio_operator () {
  (kubectl minio init) >> ./temp/setup.log 2>> ./temp/setup.log
}

function print_jwt () {
  printf "${GREEN}\n   "
  kubectl -n minio-operator get secret $(kubectl -n minio-operator get serviceaccount console-sa -o jsonpath="{.secrets[0].name}") -o jsonpath="{.data.token}" | base64 --decode
  printf "\n\n"
  printf "${YELLOW}   Minio JWT is stored in minio-jwt.txt\n${NOCOLOR}"
  kubectl -n minio-operator get secret $(kubectl -n minio-operator get serviceaccount console-sa -o jsonpath="{.secrets[0].name}") -o jsonpath="{.data.token}" | base64 --decode > minio-jwt.txt
}

function wait_on_operator () {
  (  
    kubectl wait --namespace minio-operator \
      --for=condition=ready pod \
      --selector=name=minio-operator \
      --timeout=2400s
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function create_tenant () {
    kubectl minio tenant create local-k8s \
      --servers 1 \
      --volumes 4 \
      --capacity 15Gi \
      --storage-class minio \
      --namespace minio-tenant-1
}

function fix_single_node_cluster_issues () {
  (
    kubectl apply -f ./kubernetes/minio/operator-deployment.yaml -n minio-operator
    kubectl delete pods -n minio-operator -l name=minio-operator
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function install_minio_on_kind () {
  printf "${BLUE}\n"
  printf "Install minio operator on KinD"
  printf "${NOCOLOR}\n"
  printf "   Prepare setup..."
  prepare_setup
  printf "\r   Prepare setup...${GREEN}done${NOCOLOR}\n"
  printf "   Create storage class..."
  create_storageclass
  printf "\r   Create storage class...${GREEN}done${NOCOLOR}\n"
  printf "   Create persistent volumes..."
  create_persistentvolumes $environment
  if [[ $? == 0 ]]; then
    printf "\r   Create persistent volumes...${GREEN}done${NOCOLOR}\n"
  fi
  printf "   Create ingress..."
  create_ingress
  printf "\r   Create ingress...${GREEN}done${NOCOLOR}\n"
  printf "   Init minio operator..."
  init_minio_operator
  printf "\r   Init minio operator...${GREEN}done${NOCOLOR}\n"
  printf "   Print JWT...\n"
  print_jwt
  printf "   Create tenant ...\n"
  create_tenant
  printf "   ${GREEN}Install minio operator on KinD successfully.${NOCOLOR}\n"
}

function install_minio_on_minikube () {
  printf "${BLUE}\n"
  printf "Install minio operator on minikube"
  printf "${NOCOLOR}\n"
  printf "   Prepare setup..."
  prepare_setup
  printf "\r   Prepare setup...${GREEN}done${NOCOLOR}\n"
  printf "   Create storage class..."
  create_storageclass
  printf "\r   Create storage class...${GREEN}done${NOCOLOR}\n"
  printf "   Create persistent volumes..."
  create_persistentvolumes $environment
  if [[ $? == 0 ]]; then
    printf "\r   Create persistent volumes...${GREEN}done${NOCOLOR}\n"
  fi
  printf "   Create ingress..."
  create_ingress
  printf "\r   Create ingress...${GREEN}done${NOCOLOR}\n"
  printf "   Init minio operator..."
  init_minio_operator
  printf "\r   Init minio operator...${GREEN}done${NOCOLOR}\n"
  printf "   Print JWT...\n"
  print_jwt
  printf "   Fix single node issues ..."
  fix_single_node_cluster_issues
  printf "\r   Fix single node issues...${GREEN}done${NOCOLOR}\n"
  printf "   ${GREEN}Install minio operator on minikube successfully.${NOCOLOR}\n"
}

function install_minio_on_local () {
  printf "${BLUE}\n"
  printf "Install minio operator on local"
  printf "${NOCOLOR}\n"
  printf "   Prepare setup..."
  prepare_setup
  printf "\r   Prepare setup...${GREEN}done${NOCOLOR}\n"
  printf "   Create storage class..."
  create_storageclass
  printf "\r   Create storage class...${GREEN}done${NOCOLOR}\n"
  printf "   Create persistent volumes..."
  create_persistentvolumes $environment
  if [[ $? == 0 ]]; then
    printf "\r   Create persistent volumes...${GREEN}done${NOCOLOR}\n"
  fi
  printf "   Create ingress..."
  create_ingress
  printf "\r   Create ingress...${GREEN}done${NOCOLOR}\n"
  printf "   Init minio operator..."
  init_minio_operator
  printf "\r   Init minio operator...${GREEN}done${NOCOLOR}\n"
  printf "   Print JWT...\n"
  print_jwt
  printf "   Fix single node issues ..."
  fix_single_node_cluster_issues
  printf "\r   Fix single node issues...${GREEN}done${NOCOLOR}\n"
  printf "   ${GREEN}Install minio operator on local successfully.${NOCOLOR}\n"
}

if [[ "$environment" != 200 ]] && [[ "$environment" == "kind" ]]; then
  install_minio_on_kind
elif [[ "$environment" != 200 ]] && [[ "$environment" == "minikube" ]]; then
  install_minio_on_minikube
elif [[ "$environment" != 200 ]] && [[ "$environment" == "microk8s" ]]; then
  printf "\n${YELLOW}Min-IO installation skipped. Min-IO operator does not work on microk8s yet.${NOCOLOR}\n"
elif [[ "$environment" != 200 ]] && [[ "$environment" == "local" ]]; then
  install_minio_on_local
else
  printf "\n${RED}Environment $environment unknown. Use 'kind', 'minikube', 'microk8s' or local.${NOCOLOR}\n"
fi