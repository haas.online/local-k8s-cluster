#!/usr/bin/env bash
set -o errexit

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

function apply_fix () {
  # Due to the following issue https://github.com/prometheus-community/helm-charts/issues/1500
  # There is a workaround implemented according to the following blog entry:
  # https://blog.ediri.io/kube-prometheus-stack-and-argocd-how-workarounds-are-born
  kubectl create -k ./kubernetes/monitoring/
}

printf "${BLUE}\n"
printf "Setup monitoring"
printf "${NOCOLOR}\n"
printf "   Apply fix for prometheus stack issue 1500..."
apply_fix >> ./temp/setup.log
printf "\r   Apply fix for prometheus stack issue 1500...${GREEN}done${NOCOLOR}\n"
printf "   ${GREEN}Setup monitoring successfully.${NOCOLOR}\n"