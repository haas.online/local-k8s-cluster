#!/usr/bin/env bash

RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NOCOLOR='\033[0m'

environment=$1

dd_password=$(head -c 512 /dev/urandom | env LC_ALL=C tr -cd 'a-zA-Z0-9\.,#+*&' | head -c 22)
dd_secret_key=$(head -c 512 /dev/urandom | env LC_ALL=C tr -cd 'a-zA-Z0-9\.,#+*&' | head -c 128)
dd_credential_aes=$(head -c 512 /dev/urandom | env LC_ALL=C tr -cd 'a-zA-Z0-9\.,#+*&' | head -c 128)
dd_http_auth_password=$(head -c 512 /dev/urandom | env LC_ALL=C tr -cd 'a-zA-Z0-9\.,#+*&' | head -c 32)
dd_redis_password=$(head -c 512 /dev/urandom | env LC_ALL=C tr -cd 'a-zA-Z0-9\.,#+*&' | head -c 10)
dd_rabbitmq_password=$(head -c 512 /dev/urandom | env LC_ALL=C tr -cd 'a-zA-Z0-9\.,#+*&' | head -c 10)
dd_rabbitmq_erlang_cookie=$(head -c 512 /dev/urandom | env LC_ALL=C tr -cd 'a-zA-Z0-9\.,#+*&' | head -c 32)
dd_postgres_password=$(head -c 512 /dev/urandom | env LC_ALL=C tr -cd 'a-zA-Z0-9\.,#+*&' | head -c 16)
dd_postgres_replication_password=$(head -c 512 /dev/urandom | env LC_ALL=C tr -cd 'a-zA-Z0-9\.,#+*&' | head -c 10)
dd_mysql_password=$(head -c 512 /dev/urandom | env LC_ALL=C tr -cd 'a-zA-Z0-9\.,#+*&' | head -c 10)

function create_namespace () {
  (
    kubectl create namespace defectdojo
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function create_defectdojo_secrets () {
  (
    kubectl create secret generic defectdojo -n defectdojo \
      --from-literal=DD_ADMIN_PASSWORD="$dd_password" \
      --from-literal=DD_SECRET_KEY="$dd_secret_key" \
      --from-literal=DD_CREDENTIAL_AES_256_KEY="$dd_credential_aes" \
      --from-literal=METRICS_HTTP_AUTH_PASSWORD="$dd_http_auth_password" 
    kubectl create secret generic defectdojo-redis-specific -n defectdojo \
      --from-literal=redis-password="$dd_redis_password"
    kubectl create secret generic defectdojo-rabbitmq-specific -n defectdojo \
      --from-literal=rabbitmq-password="$dd_rabbitmq_password" \
      --from-literal=rabbitmq-erlang-cookie="$dd_rabbitmq_erlang_cookie"
    kubectl create secret generic defectdojo-postgresql-specific -n defectdojo \
      --from-literal=postgresql-password="$dd_postgres_password" \
      --from-literal=postgresql-postgres-password="$dd_postgres_password" \
      --from-literal=postgresql-replication-password="$dd_postgres_replication_password"
    kubectl create secret generic defectdojo-mysql-specific -n defectdojo \
      --from-literal=mysql-password="$dd_mysql_password"
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function print_defectdojo_secrets () {
  printf "${GREEN}\n"
  printf "      Admin password:      %s\n" $dd_password | tee defectdojo.txt
  printf "      HTTP-Auth password:  %s\n" $dd_http_auth_password | tee -a defectdojo.txt
  printf "      Redis password:      %s\n" $dd_redis_password | tee -a defectdojo.txt
  printf "      PostgreSQL password: %s\n" $dd_postgres_password | tee -a defectdojo.txt
  printf "${YELLOW}\n"
  printf "   Defect Dojo passwords are stored in defectdojo.txt"
  printf "${NOCOLOR}\n"
}

function create_defectdojo_configmaps () {
  (
    kubectl create cm defectdojo-ca-certs -n defectdojo --from-file=./temp/ca.crt
  ) >> ./temp/setup.log 2>> ./temp/setup.log
}

function create_defectdojo_persistentvolumes () {
    if [[ "$1" != 200 ]] && [[  "$1" == "minikube" ]]; then
      (kubectl apply -f ./kubernetes/securecodebox/persistent-volumes.yaml) >> ./temp/setup.log 2>> ./temp/setup.log
      return 0
    elif [[ "$1" != 200 ]] && [[ "$1" == "kind" || "$1" == "microk8s" || "$1" == "local" ]]; then
      printf "\n   ${YELLOW}Persistent volumes are not installed on KinD, microK8s or local. Skip step.${NOCOLOR}\n"
      return 1
    else
      printf "   ${RED}\nEnvironment $1 unknown. Use 'kind', 'minikube', 'microk8s' or 'local'.${NOCOLOR}\n"
      return 1
    fi
}

printf "${BLUE}\n"
printf "Setup secure code box"
printf "${NOCOLOR}\n"
printf "   Create Defect Dojo namespace..."
create_namespace
printf "\r   Create Defect Dojo namespace...${GREEN}done${NOCOLOR}\n"
printf "   Create Defect Dojo secrets..."
create_defectdojo_secrets
printf "\r   Create Defect Dojo secrets...${GREEN}done${NOCOLOR}\n"
printf "   Save and print Defect Dojo passwords...\n"
print_defectdojo_secrets
printf "   Create Defect Dojo configmaps..."
create_defectdojo_configmaps
printf "\r   Create Defect Dojo configmaps...${GREEN}done${NOCOLOR}\n"
printf "   Create Defect Dojo persistent volumes..."
create_defectdojo_persistentvolumes $environment
if [[ $? == 0 ]]; then
  printf "\r   Create Defect Dojo persistent volumes...${GREEN}done${NOCOLOR}\n"
fi
printf "   ${GREEN}Setup secure code box successfully.${NOCOLOR}\n"