#!/usr/bin/env bash
set -o errexit

RED='\033[1;31m'
NOCOLOR='\033[0m'

environment=$1

if [[ "$environment" != 200 && "$environment" == "kind" ]]; then
  docker unpause local-k8s-cluster-control-plane \
    local-k8s-cluster-worker \
    local-k8s-cluster-worker2 \
    local-k8s-cluster-worker3 \
    local-k8s-registry
elif [[ "$environment" != 200 && "$environment" == "minikube" ]]; then
  minikube unpause
  minikube mount ~/docker:/host
else
  printf "${RED}Environment $environment unknown. Use 'kind' or 'minikube'.${NOCOLOR}\n"
fi